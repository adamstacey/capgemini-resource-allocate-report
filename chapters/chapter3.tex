%!TEX root = ../dissertation.tex

\chapter{Preparation - Tools, Technical Design \& Architecture}

The following sections dives into the technical aspects of Capgemini Resource Allocate, decisions and justification for the tooling used to achieve the software solution. Capgemini delivers a vast and varied range of software solutions for its clients, and being a global leader in the software industry Capgemini has a keen focus on ensuring its up to date with current technologies. Specifically the fast paced movement of JavaScript over the past couple years, in the words of Tom Phethan:

\begin{quote}
  "In the last few years we have seen an explosion in the demand for bespoke-built, high-quality, interactive user interfaces. At Capgemini, we see this manifested as increased demand for frontend development skills, specifically modern JavaScript frameworks such as React, Angular, Vue or Ember. Alongside this, the predilection for “JavaScript Everywhere” has resulted in us developing with React Native for cross-platform mobile apps, Node.js for server-side and lightweight APIs, and the Serverless framework for deploying Functions-as-a-Service among numerous others I won’t list here." \cite{CapgeminiJacaScriptEverywhere}
\end{quote}

As a front end developer myself at Capgemini JavaScript and modern web frameworks is a great interest of mine, the individual project Capgemini Resource Allocate will be a great place to investigate and learn developing and integrating these modern web frameworks to deliver the overall solution, and this fulfils the project goal of running the project as Capgemini would run a project for its clients.

\section{Tools}

The tools used for Capgemini Resource Allocate will be split into 3 sections, the Client, Server, Testing and General best practice. The decisions made here are a result of the prior investigations and planning work carried out within sections 1 \& 2 of this paper.

\subsection{Client}

The 'Client' is everything that the users will interact with. In the case of Capgemini Resource Allocate this encapsulates the web application, iOS, and Android mobile applications and everything required to build and develop these platforms. The first big question to answer is what JavaScript framework to use?

\subsubsection{Javascript Frameworks}

Tom Phethan mentioned four JavaScript frameworks which are commonly used in Capgemini, React Angular, Vue or Ember for this project I want to ensure I'm working in technologies that have the highest trends in the industry today, using 'NPM Trends' \citep{NPMTrends} a tool that allows users to see the trend in library downloads of a project.

\begin{figure}[h]
  \centering
  \includegraphics[width=1\textwidth]{figures/chapter3/npm_trends.png}
  \captionsetup{justification=centering}
  \caption{NPM Trends of JavaScript Frameworks
  \label{fig:understanding_the_problem_flow}}
\end{figure}

From the trend figure 3.1 over the past year ReactJS towers above the other JavaScript frameworks in downloads, meaning ReactJS will be used for Capgemini Resource Allocate, however "React is a library that’s designed to be one piece of the puzzle. React provides a thin view layer and leaves it to the developer to choose the remaining pieces of the architecture." \citep{ReactJSViewOnly} The next thing now the view layer is decided is the model and controller, Redux is the next logical tool for state management of a ReactJS applications, "Redux is a predictable state container for JavaScript apps."

\begin{figure}[h]
  \centering
  \includegraphics[width=0.6\textwidth]{figures/chapter3/react_redux.png}
  \captionsetup{justification=centering}
  \caption{Redux state management process
  \label{fig:understanding_the_problem_flow}}
\end{figure}

Redux is a defacto standard library for managing state in React apps. Redux has a single store that holds all data centrally, which makes debugging easier. Unidirectional data flow that makes behaviour of app consistent and predictable.

Effectively the flow chart figure 3.2 describes the Redux architecture with ReactJS, the View Components trigger an action, typically by user click on a form submit button for example, that action will trigger something such as an API call and return the response to a Reducer, that reducer will manipulate the applications state with the new data, and publish that to the store for the View component to display the new data.


\subsubsection{Dependency Management}

Software installation is done with a 'package manager'. The package manager gets software packages from repositories, solves the dependencies and installs them onto your system. The package manager also makes it easy to remove packages later down the line or update them. This gives a lot of control to prevent software mismatches and missing prerequisites.

Package Dependencies are the different packages you as a developer need for the system to run, or for you to be able to develop. The package manager used in Capgemini Resource Allocate will be NPM. NPM is a NodeJS package manager, which is a command-line utility for interacting with repositories that aid in package installation, version management, and dependency management. These applications or packages can be found on the NPM website \cite{NPM} using the find packages search bar.

All npm packages contain a file, usually in the project root, called package.json. This file holds various metadata relevant to the project. This file is used to give information to npm that allows it to identify the project as well as handle the project dependencies. It can also hold other meta data such as project description, licence information, the version of the project, even configuration data all of which can be vital to both npm and to the end users of the package. All software for Capgemini Resource Allocate will have a package.json file in the root at least which will list all dependencies used. The top packages that will be used are:

\begin{center}
  \begin{tabular}{ c c c c c }
   react & redux & firebase & cordova \\
   superagent & moment & lodash & redux-devtools  \\
   eslint & react-google-maps & foundation-sites & react-router
  \end{tabular}
\end{center}

The full package.json can be found in Appendix: \ref{AppendixB}

\subsubsection{CSS Framework}

A CSS framework is a library that has a lot of options for you to use in your HTML development, potentially making it faster and easier for you to develop your website or web app. Capgemini Resource Allocate will make use of Zurb Foundation Sites "we built Foundation for Sites to be the most advanced responsive front-end framework in the world" \citep{ZurbSites}, this is a pretty bold claim by Zurb however I have over 5 years experience with the framework and it really helps aid rapid prototyping, cross browser functionality, enforcing good web design, semantics and accessibility.

The use of Zurb Foundation Sites will also help when targeting styles for the mobile platform which Capgemini Resource Allocate needs to be available on as per requirements. The framework adopts a mobile first approach which is where the mobile device is the initial target for styles, and with the use of media queries to define pixel width ranges the styles and layouts can be altered for the best representation of content for that viewport.

The main features from Zurb Foundation that will be used in Capgemini Resource Allocate will be the XY Grid, Normalisation, Buttons, Sub Navigation, Typography and Helper Classes, other aspects of the framework can be overrided or extended with general CSS or SCSS, which is a more powerful functional language that compiles to CSS.

\subsubsection{Cordova}

"Apache Cordova is an open-source mobile development framework. It allows you to use standard web technologies - HTML5, CSS3, and JavaScript for cross-platform development. Applications execute within wrappers targeted to each platform, and rely on standards-compliant API bindings to access each device's capabilities such as sensors, data, network status, etc." \citep{CordovaAbout}

To turn Capgemini Resource Allocate into a hybrid mobile application the root of the project requires a config.xml file. This file acts similar to the package.json for NPM. The config.xml file holds various metadata relevant to the project, what platforms to package and build the web application into, any additional plugins required, any media assets for launch icons and splash screens, and any device behaviour settings.

The full config.xml can be found in Appendix: \ref{AppendixC}

\subsection{Server}

The beackend for Capgemini Resource Allocate will be exploring 'Serverless frameworks' as mentioned by Tom Phethan, the tool which will be used is Google Firebase \citep{Firebase}. This section will cover all the backend tools required to support the Capgemini Resource Allocate Client Applications.

\subsubsection{Firebase}

Google Firebase will be the foundation of Capgemini Resource Allocate, "Firebase is all of Google's strengths wrapped into a neat platform for developers." \cite{FirebaseQuote} Firebase is a platform that will allow you to develop apps quickly. It offers a number of different services built-in, including some basic analytics. Capgemini Resource Allocate will be utilising the following services:

\begin{itemize}
  \item Realtime database: "the Firebase Realtime Database is a cloud-hosted database. Data is stored as JSON and synchronized in realtime to every connected client." \citep{FirebaseDB} This is where the data exports, DCS \& ADAO will be transformed and uploaded to.
  \item Authentication: "Firebase Authentication provides backend services, easy-to-use SDKs, and ready-made UI libraries to authenticate users to your app." \citep{FirebaseAuth}. This service will allow Capgemini Resource Allocate to have sign in functionality which will grant access to the system to authorised users only. Unfortunately it was not possible to integrate with Capgemini SSO as they do not have a development sandbox for adhoc projects such as this. However Firebase can cater for third party authentication systems if this project progresses into something Capgemini would like to invest in.
  \item Cloud Functions: "Cloud Functions for Firebase lets you automatically run backend code in response to events triggered by Firebase features and HTTPS requests." \citep{FirebaseCloudFunc} Capgemini Resource Allocate will have cloud functions to trigger database updates and actions to fulfil system requirements around messaging, emailing and general data transformation upon interactions with the UI. The cloud functions can be found in the folder \lstinline{functions/} in the Capgemini Resrouce Allocate codebase.
  \item Hosting: "Firebase Hosting is production-grade web content hosting for developers. With Hosting, you can quickly and easily deploy web apps and static content to a global content-delivery network" \citep{FirebaseHost}. Capgemini Resource Allocate will be hosted to, \href{https://capgemini-resource-allocate.firebaseapp.com/}{Capgemini Resource Allocate}
\end{itemize}

Google Firebase has a clean easy to use dashboard which has quick access to all the features you'll need right in the main sidebar the Firebase dashboard for Capgemini Resource Allocate can be seen in Appendix \ref{AppendixD}

\subsubsection{Third Party API's}

To meet the requirements there will need to be interaction with additional API's, mainly Googles API's around deriving locations for employees, roles and clients. Additionally for Smart Allocation of Capgemini Resource Allocate further information such as travel distances and times will needs to be retreived to successfully derive potential swap Candidates. The following two API's will retrieve this information:

\begin{center}
  \begin{tabular}{ p{4cm} p{4cm} p{5cm} }
    \hline
    API & Parameters & Value \\
    \hline
    GET Geocoding API & address | key & postcode, full address, latitude or longitude | API key \\
    GET Distance Matrix API & origin | destination | mode | transit\_mode | key & lat lng | lat lng | driving or transit | rail | API key \\
  \end{tabular}
\end{center}

Further usage guides for \href{https://developers.google.com/maps/documentation/geocoding/start}{Geocoding API} \& \href{https://developers.google.com/maps/documentation/distance-matrix/start}{Distance Matrix API}.



\subsubsection{Security}

Capgemini Resource Allocate will hold information that requires security for a few reasons, firstly the web application will be storing personal information about Capgemini's employees and their home locations, it is vital for any business to keep safe and secure as per the \href{http://www.legislation.gov.uk/ukpga/1998/29/contents}{Data Protection Act}, secondly Capgemini Client and Role information needs to be secure against competitors Capgemini are regularly in competition and if competitor organisations had access to resource information this could be damaging for bid processes.

Measures in place for the security of Client Application: Initializing the Firebase application meaning only aplications that have the Capgemini Resource Allocate API key can allow communication with the Realtime Database and Authorisation services. As well as user based authorisation, when users login via the UI the Client requests authorisation to the Firebase Authorisation Service, with a username (email) and password. The Authorisation Service then checks if the users is valid and returns a successful login with Authorisation token to be used to communicate with other Firebase services of Capgemini Resource Allocate. As well as this there is no use of global variables which can be accessed from the browser can result in accessing information

Measures in place for the security of Server Application: the backend functionality of Capgemini Resource Allocate will reject any communications that does not have a valid API key of application and user keys. And access to Capgemini Resource Allocate Firebase Console is protected by username and password.

\subsection{Testing}

To ensure functionality of Capgemini Resource Allocate satisfies the requirements and hsa good functional code. Testing of software is extremely important as it points out defects that my be introduced during development phases. The testing of Capgemini Resource Allocate will be carried out in a Test Driven Development (TDD) style for the unit tests written with 'Jest', and in a Behaviour Driven Development (BDD) style for functional end to end scenario based testing written with 'Nightwatch'.

Capgmeini Resource Allocate codebase will pick up any files with the filename \lstinline{<filename>.test.js} in the folder \lstinline{src/} and for end to end tests they can be found within the folder \lstinline{e2e-test}.

\subsubsection{Unit Testing}

"Jest is used by Facebook to test all JavaScript code including React applications" \citep{Jest} Capgemini Resource Allocate will have unit testing for the Redux Actions and Reducers, these two components focus on the applications state a state manipulation based on user interactions with the UI the structure of a Jest test can be seen in the snippet below:

\begin{lstlisting}[language=JavaScript]
  describe('Edit actions', () => {
    it('Should clear the loaded form in preparation for new load', () => {
      const expectedAction = { type: 'CLEAR_LOAD_FORM' };
      expect(actions.clearLoadForm()).toEqual(expectedAction);
    });
  });
\end{lstlisting}

What this snippet is doing is defining a test, and every \lstinline{it} is a test case, this test case is testing with an expect assertion library to ensure the \lstinline{clearLoafForm()} triggers a Redux action to clear the redux form, this will then get picked up by the Redux reducer and clear the form:

\begin{lstlisting}[language=JavaScript]
  describe('Form Reducer', () => {
    it('Should clear the loaded form from application state', () => {
      expect(reducer({}, { type: 'CLEAR_LOAD_FORM' })
        .loadForm.data).toEqual(null);
    });
  });
\end{lstlisting}

To keep inline with TDD best practices, the unit tests will be written first to fail, then functionality will be added to assert passes of the tests, by thinking about the tests first this helps drive successful functionality. Additionally the tests re-run with every edit to any JavaScript file, Jest has a test runner watcher that will rerun the tests on file changes.

Now that the internal state management functionality is unit tested the actual user functionality of the application needs to be tested as the next section details the tool and process for the feature regression testing of the application.

\subsubsection{End to End}

"Nightwatch.js is an easy to use Node.js based End-to-End (E2E) testing solution for browser based apps and websites. It uses the powerful W3C WebDriver API to perform commands and assertions on DOM elements" \citep{Nightwatch}. Capgemini Resource Allocate will be using Nightwatch to test all the user interactions, which can be derived before writing any code just from the requirements, the requirements can be converted into scenarios to test the user flows the following are the BDD test plan scenarios created which will be developed to establish correct functionality for happy flows.

\begin{center}
  \begin{tabular}{ p{3cm} p{10cm} }
    Login & As a user i cant click login button without entering values \\
    & As a user i can login to Capgemini Resource Allocate \\
    & As a user i cannot login with invalid details \\
    Employee Process & As a user i want to be able to add a employee \\
    & As a user i can filter, view, edit, and delete employee \\
    Role Process & As a user i want to be able to add a role \\
    & As a user i can filter, view, edit, and delete role \\
    Client Process & As a user i want to be able to add a client \\
    & As a user i can filter, view, edit, and delete client \\
    Navigation & As a user i navigate through all sections of the application using the menu \\
    Map & As a user i navigate to the maps section and view the google map and i can see the filter section \\
    & As a user i navigate to the maps section and view the google map and i can see all the employees, clients, and roles \\
    & As a user i navigate to the maps section and view the google map and i can the distance difference between the potential swap employees \\
    Swap & As a user i want to be able to add employees for swap \\
    & As a user i want to see a list of the potential swap employees
  \end{tabular}
\end{center}


With the use of Nightwatch the test plan can be automated saving time from not having to run the tests manually. These tests fire up a standalone Chrome web browser and run the interactions as if the automation tool is the user, to \lstinline{assert} successful or unsuccessful tests. The code snippet below shows an example of the feature test written in JavaScript.

\newpage

\begin{lstlisting}[language=JavaScript]
  module.exports = {
    'As a user i want to be able to add a client': function (browser) {
      browser
        .url('https://capgemini-resource-allocate.firebaseapp.com/')
        .waitForElementVisible('body', 1000)
        .setValue('input[type=email]', 'astacey1991@googlemail.com')
        .setValue('input[type=password]', 'crapassword')
        .waitForElementVisible('button[class=button]', 1000)
        .click('button[class=button]')
        .pause(5000)
        .assert.containsText('.app-date', 'Capgemini Resource Allocate')
        .click('#menuToggle')
        .pause(510)
        .click('#navigationClients')
        .refresh()
        .pause(6000)
        .waitForElementVisible('button[id=addClient]', 1000)
        .click('button[id=addClient]')
        .pause(1000)
        .assert.containsText('#lightboxTitle', 'Add Client')
        .setValue('input[name=location]', 'Croydon')
        .setValue('input[name=name]', 'RS Heating')
        .click('a[id=findLocation]')
        .pause(4000)
        .assert.containsText('#locationValue', '13 Pagehurst Rd, Croydon CR0 6NS, UK')
        .setValue('input[name=projectName]', 'lorem')
        .click('select[name=type] option[value=Onshore]')
        .click('button[type=submit]')
        .end();
    }
  }
\end{lstlisting}

\subsubsection{Reporting}

Reporting is a great tool for visualising testing coverage of lines of code, branches, and function, as well as error handling and measuring performance of Firebase Services. As Capgemini Resource Allocate is using a free tier of Firebase the reporting tools around thresholds and quotas of usage will be helpful for development and monitoring performance at the same time.

Jest unit testing framework can be extended to report coverage of code either through the terminal or generated HTML reporting which can be found in the folder \lstinline{coverage/lcov-report/} open the \lstinline{index.html} in your browser.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.7\textwidth]{figures/chapter3/coverage.png}
  \captionsetup{justification=centering}
  \caption{Jest coverage CLI reporter
  \label{fig:jest_coverage}}
\end{figure}

The following are examples of firebase reporting tools, the google console dashboard that shows errors, api timings, and usages. Firebase Database usage, connections, and storage quotas, and lastly Firebase cloud function usage.

\begin{figure}[h]
  \centering
  \includegraphics[width=1\textwidth]{figures/chapter3/firebase_reporting.png}
  \captionsetup{justification=centering}
  \caption{Firebase reporting screen shots
  \label{fig:firebase_reporting}}
\end{figure}

\subsection{General Project Best Practice}

This section is about how the project should be carried out from a best practice point of view, Capgmeini have a high standard of working, and this should reflect in the individual project. There are other ways of ensuring good code, and project practices to increase maintainaiblity of code and correctness, the following tools and princples will be a part of the Capgmini Reosource Allocate workflow.

\subsubsection{Version Control (VCS)}

"A version control system (also known as a Revision Control System) is a repository of files, often the files for the source code of computer programs, with monitored access. Every change made to the source is tracked, along with who made the change, why they made it, and references to problems fixed, or enhancements introduced, by the change." \citep{Git} Capgemini Resource Allocate will be sourced controlled with \href{https://gitlab.com/}{GitLab}. GitLab offers everything typical VCS do, however it has an integrated CI environment which can build, package, and deploy the source code. Having the source code in GitLab adds a layer of protection and a way of handling changes. All Capgemini projects will use a VCS regardless of size of the project.

\subsubsection{CI/CD}

Continuous Integration (CI) is a software development practice that advocates the frequent merge of different working copies into a shared branch. Every merge triggers a new automated build, which is verified by unit tests. GitLab CI/CD does just this, with the use of a configuration file \lstinline{.gitlab-ci.yml}, another CI tool \href{https://www.bitrise.io/}{Bitrise} will be used for building the iOS and Android applications and packaging the build artefacts for distribution. The build pipeline for Capgemini Resource Allocate through a sequence diagram will be:

\begin{figure}[h]
  \centering
  \includegraphics[width=0.8\textwidth]{figures/chapter3/CI_Workflow.png}
  \captionsetup{justification=centering}
  \caption{CI Workflow for Capgemini Resource Allocate
  \label{fig:CI_Workflow}}
\end{figure}

Benefits of having the source code built and packaged with CI is if any changes cause breaking effects to the system they will be picked up by the build resulting in a failure, if there is a failure the code cannot be merged from feature branches to production protected branches such as the \lstinline{master} branch.

\subsubsection{Eslint}
Eslint is a linting tool to ensure code it written to a maintainable standard and meets the general industry open source agreed standard for writing ReactJS applications, ES6 JavaScript, and accessibility recommendation helpers. Capgemini Resource Allocate will use eslint as a part of the build and development runner which will cause failures at compile time which will need to be resolved. Figure 3.6 shows an example of typical reporting eslint will display:

\begin{figure}[h]
  \centering
  \includegraphics[width=1\textwidth]{figures/chapter3/eslint.png}
  \captionsetup{justification=centering}
  \caption{Eslint error log
  \label{fig:eslint}}
\end{figure}

\section{Design}

This section includes diagrams of the overall architecture, other important details, and user interface wifreframes.

\subsection{Technical}

This section includes the database design, API design, and overall architecture design.

\subsubsection{Database Structure}

The JSON Firebase Realtime Database structure will be:

\begin{lstlisting}[language=JavaScript]
  "capgemini-resource-allocate": {
    "activity": { "<uid>": { /* action description & info */ } }
    "archive": { "role": {}, "swap": {} /* archived data */ }
    "clients": { "<client code>": { /* client details */} },
    "employees": { "<employee number>": { /* employee details */} },
    "formList": { /* backing data for form selection */ },
    "reports": { /* hold reporting info by date */ },
    "resourceSwap": { "candidates": [ { /* employees that can be swapped */ } ]},
    "roles": { "<role number>": { /* role details */} },
    "upload" { "potentialEmployees": { /* Cloud functions to watching for changes */ } }
  }
\end{lstlisting}

Each role, employee, and client has a unique id for the object reference, by doing this it optimises the NoSQL database for requesting individual data instead of having to iterate through arrays to pick out the correct values.

\subsubsection{APIs}

The web application and mobile applications will be using websockets for the connection with Firebase Realtime DB however the database can still be accessed via REST API calls, which will need to be utilised for some initial load aspects of the data. Converting the object structure to forward slashes will result in the path to request. For example:

\begin{lstlisting}[language=JavaScript]
  "capgemini-resource-allocate": {
    "employees": { "1234": { /* employee details */} }
  }
\end{lstlisting}

The path to request would be to retrieve the employee details of '1234':

\begin{center}
  \begin{tabular}{ p{3cm} p{5cm} p{6cm} }
    \multicolumn{3}{|c|}{https://capgemini-resource-allocate.firebaseio.com/employees/1234.json} \\
    \hline
    Type & Parameters & Request Body \\
    \hline
    GET & auth='auth-key' & none \\
    POST & auth='auth-key' & details to save \\
    PUT & auth='auth-key' & details to save \\
    DELETE & auth='auth-key' & none \\
  \end{tabular}
\end{center}

Additional API's are Google Geocoding and Distance Matrix defined previously in 'Third Party API's' section of this paper.

\subsubsection{Overall Architecture}

\begin{figure}[h]
  \centering
  \includegraphics[width=1\textwidth]{figures/chapter3/overall_arc.png}
  \captionsetup{justification=centering}
  \caption{Capgemini Resource Allocate Architecture
  \label{fig:arc}}
\end{figure}

\subsection{User Interface (UI) - Wireframes}

The following designs are the views for Capgemini Resource Allocate, these are loose wireframes which look into ensuring the best user experience (UX), as development continues the Capgemini Brand will start to be implemented, however the most important aspect trying to be solved here is the UX.

\begin{figure}[h]
  \centering
  \includegraphics[width=1\textwidth]{figures/chapter3/mobileUI.png}
  \captionsetup{justification=centering}
  \caption{Mobile UI
  \label{fig:mobileUI}}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=1\textwidth]{figures/chapter3/desktopUI-1.png}
  \captionsetup{justification=centering}
  \caption{Desktop UI
  \label{fig:desktop1}}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=1\textwidth]{figures/chapter3/desktopUI-2.png}
  \captionsetup{justification=centering}
  \caption{Desktop UI continued
  \label{fig:desktop2}}
\end{figure}

The wireframes are not specific to roles, employees, and clients this is because the principle workflow is the same for each UI component resource managers need to be able to view, create, edit, and delete these records, therefore for the best user experience this UI will be similar ensuring the learnability is a simple as possible.
