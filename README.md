# Capgemini Resource Allocate Report

## Getting started
1. Install LaTeX. For Mac OS X, we recommend MacTex (http://tug.org/mactex/); for Windows, MiKTeX (http://miktex.org/); and for Ubuntu, Tex Live (`sudo apt-get install texlive-full`)
2. Install the default fonts: EB Garamond, Lato, and Source Code Pro. The files are provided in `fonts/EB Garamond`, `fonts/Lato`, and `fonts/Source Code Pro`.
3. Personalize the document by filling out your name and all the other info in `frontmatter/personalize.md`.
4. Build your dissertation with `build.command`, located in the `scripts` directory (e.g., you can `cd` into the main directory and then run `./scripts/build.command`).

## FAQ

### How do I make the text justified instead of ragged right?
Remove or comment out the line `\RaggedRight` from the .cls file.

## This report
### Building pdf
1. Install LaTex. (`brew install mactex`, `sudo apt-get install texlive-full`)
2. Install the fonts used in this template. They can be found in fonts/ directory
3. From the main directory run `scripts/build.sh`
4. Review logs for problems. PDF is output to the directory the command is ran in.

Template sourced from [http://dissertate.io/](http://dissertate.io/)